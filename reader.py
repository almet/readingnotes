import datetime
from pathlib import Path
import yaml
from dataclasses import dataclass, field
from collections import defaultdict
from typing import List
import re
import codecs
import os
import shutil
import sys

from jinja2 import Environment, FileSystemLoader
from slugify import slugify


THEME_PATH = "theme"


@dataclass
class Note:
    note: str
    tags : List[str] = field(default_factory=list)
    author: str = ""


@dataclass
class Reading:
    title: str
    author: str
    read_on: datetime.datetime
    notes: List[Note]
    tags: list = field(default_factory=list)
    subtitle: str = ""
    url: str = ""
    isbn: str = ""
    cover: str = ""

    @staticmethod
    def load_from_yaml(filename):
        parsed = yaml.safe_load(filename.read_text())
        return Reading(**parsed)

    @property
    def slug(self):
        return slugify(self.title)

    @property
    def image_url(self):
        if self.cover:
            return self.cover
        if self.isbn:
            return f"http://covers.openlibrary.org/b/isbn/{{ self.isbn }}-L.jpg"
        return False


def postprocess_readings(readings):
    for reading in readings:
        for note in reading.notes:
            if 'tags' not in note:
                note['tags'] = []
            note['tags'].extend(reading.tags)
            note['reading'] = reading


def build_tags(readings):
    tags = defaultdict(list)
    for reading in readings:
        for note in reading.notes:
            for tag in note['tags']:
                tags[tag].append(note)
    return tags


class WebsiteGenerator:
    def __init__(self, output_path, theme_path, readings, tags):
        self.output_path = output_path
        self.theme_path = theme_path
        self.readings = readings
        self.notes = []
        self.tags = tags
        for reading in readings:
            self.notes.extend(reading.notes)
        
        self._env = Environment(
            loader=FileSystemLoader(self.theme_path + "/templates"))

    def generate_website(self):
        self.create_paths(
            self.output_path,
            self.output_path + "/readings",
            self.output_path + "/assets",
            self.output_path + '/tags')
        
        self.copy_assets()

        self.render_template(
            "index.html",
            "index.html",
            readings=self.readings)
        
        # Readings
        for reading in self.readings:
            self.render_template(
                "reading.html",
                f"readings/{reading.slug}.html",
                reading=reading)
        
        # Tags
        for tag, notes in self.tags.items():
            self.render_template(
                "tag.html",
                f"tags/{tag}.html",
                tag=tag, notes=notes)

        
    def copy_assets(self):
        copy(
            os.path.join(self.theme_path, 'assets'),
            os.path.join(self.output_path, 'assets'))
    

    def render_template(self, tpl_name, filename, **options):
        template = self._env.get_template(tpl_name)
        output = template.render(**options)

        full_path = os.path.join(self.output_path, filename)

        with codecs.open(full_path, 'w+', encoding='utf-8') as f:
            f.write(output)

    def create_paths(self, *paths):
        def _create_path(path):
            if not os.path.exists(path):
                os.makedirs(path)
        list(map(_create_path, paths))


def copy(source, destination):
    """Recursively copy source into destination.
    Taken from pelican.
    If source is a file, destination has to be a file as well.
    The function is able to copy either files or directories.
    :param source: the source file or directory
    :param destination: the destination file or directory
    """
    source_ = os.path.abspath(os.path.expanduser(source))
    destination_ = os.path.abspath(os.path.expanduser(destination))
    
    if os.path.isfile(source_):
        dst_dir = os.path.dirname(destination_)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        shutil.copy2(source_, destination_)

    elif os.path.isdir(source_):
        if not os.path.exists(destination_):
            os.makedirs(destination_)
        if not os.path.isdir(destination_):
            return

        for src_dir, subdirs, others in os.walk(source_):
            dst_dir = os.path.join(destination_,
                                   os.path.relpath(src_dir, source_))

            if not os.path.isdir(dst_dir):
                # Parent directories are known to exist, so 'mkdir' suffices.
                os.mkdir(dst_dir)

            for o in others:
                src_path = os.path.join(src_dir, o)
                dst_path = os.path.join(dst_dir, o)
                if os.path.isfile(src_path):
                    shutil.copy2(src_path, dst_path)



def process(path=".", output="output"):
    readings = list(map(Reading.load_from_yaml, Path(path).glob('notes-*.yaml')))
    postprocess_readings(readings)
    tags = build_tags(readings)
    generator = WebsiteGenerator(output, "theme", readings, tags)
    generator.generate_website()

process(sys.argv[1], sys.argv[2])